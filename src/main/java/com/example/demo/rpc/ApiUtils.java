package com.example.demo.rpc;


import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * api 封装 工具
 *
 * @author yzd
 */
public class ApiUtils {

	private static final String UTF_8 = "utf-8";

	public static byte[] convertBase64ToBytes(String str_base64) {
		try {
			byte[] decode = Base64.getDecoder().decode(str_base64.getBytes(UTF_8));
			return decode;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static String convertBytesToBase64(byte[] BytesData) {
		String basicEncoded = Base64.getEncoder().encodeToString(BytesData);
		//String urlEncoded = Base64.getUrlEncoder().encodeToString(ordinal.getBytes(UTF_8));
		//URL安全的base64编码器
		return basicEncoded;
	}

	/**
	 * 请求 语音转写 服务器
	 *
	 * @param token token
	 * @param fs 采样率
	 * @param inputData wav 文件流.
	 * @return 转写结果
	 */
	public static String sendToServer(String token, int fs, short[] inputData) {

		String url = "http://10.39.65.179:20000/";

		String _fs = String.valueOf(fs);
		String postData = "token=" + token + "&fs=" + _fs;

		StringBuffer buf = new StringBuffer();
		buf.append(postData);

		for (int i = 0; i < inputData.length; i++) {
			buf.append("&wavs=").append(String.valueOf(inputData[i]));
		}
		postData = buf.toString();
		return HttpRequest.sendPost(url, postData);
	}

}

